$('.sl').slick({
  dots: true,
  infinite: false,
  speed: 50,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
	}
  ]
});